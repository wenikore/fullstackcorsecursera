para utilizar  nativescript de forma global debe ser usada con node version 10 a  la 14
 
 	npm install -g nativescript  

para crear un proyecto con 
	
	tns nombreproeycto
	tns create MyApp --template tns-template-blank 

Aquí le está pasando dos cosas al comando crear: MyApp, que determina el nombre de la aplicación
que está creando, y la opción --template, que le dice a la CLI de Nativescript que realice un
andamiaje (scaffolding) de una aplicación, usando una plantilla predefinida llamada “plantilla-tns -
blanco". 

para ejecutar 

	tns run


Si ejecutas el comando 
	
	“tns device”,

verás el listado de los dispositivos conectados y que se pueden usar con 
	“tns run” o “tns debug”. 
	Verás los dispositivos reales conectados por USB o bien los emuladores que estén iniciados, es decir en ejecución en tu máquina. 

Luego, desde Android Studio, accede a Herramientas o Tools, luego a AVD Manager, y cliquea Play
para iniciarlo.
Una vez iniciado el emulador, ya puedes ejecutar:
	1. tns run android
	2. tns debug android 