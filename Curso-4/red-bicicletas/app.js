'use strict'; 
require('newrelic')
var sessionstorage = require('sessionstorage');
require('dotenv').config(); //utilizar variables de ambiente
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

//### Manejo de logeo###
const session = require('express-session');
const passport = require('./config/passport')
const Usuarios = require('./models/usuarios');
const Token    = require('./models/token');
const mongoDBStore=require('connect-mongodb-session')(session);
//### monejo de tokens
var jwt = require('jsonwebtoken');
//guardar session en memoria en el servidor
//const store = new session.MemoryStore;
//#######conection MongoDB#######
var mongoose = require('mongoose');
//var mongoDB = "mongodb://localhost:/red_bicicletas" //DBpruebas->testdbbiciletas DBprodu ->red_bicicletas
let store
if(process.env.NODE_ENV ==='development'){
  console.log('utilizando ambiente local')
  var mongoDB = process.env.MONGO_URI;
  store = new session.MemoryStore;
}else{
  console.log('utilizando ambiente PRD')
  var mongoDB = process.env.MONGO_URI_PRD;
  //se maneja la session con mongoDB
  store = new mongoDBStore({
    uri: mongoDB,
    collection:'sessions'
  });
  store.on('error',function(error){
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();
//cifrado de token semilla
app.set('secretKey','jwt_pwd_!!223344');
//configuracion en express servido que serian las cookis
app.use(session({
  cookie: {
    maxAge: 240 * 60 * 60 * 1000
  }, //expiracion de session 10 dias
  store: store, //guardar la session
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_biciletas_!!!***!"-!"-!"-!"-!"-!"-!-123123' //codigo para generar id de la cookie encritaba y sus datos
}));

mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongoDB conection error'));
db.once('open', () => {});
//#######END MongoDB#######


//####  Router ######
var indexRouter = require('./routes/index');
var bicicletasRouter = require('./routes/bicicletas');
var usuariosRouter = require('./routes/usuariosRoutes');
var tokenRouter = require('./routes/tokenRouter');
var loginRouter = require('./routes/loginRouter');
var registerRouter = require('./routes/register');
//####END Router#####

//#######API#######
var authApiRouter = require('./routes/API/authRouter');
var bicicletasRouterAPI = require('./routes/API/bicicletasRouter');
var usuarioRouterAPI = require('./routes/API/usuarioRouter');
//#######END#######

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

//##########LOGIN##################

app.post('/login', function (req, res, next) {
  passport.authenticate('local', function (err, usuario, info) {
    if (err) return next(err);
    if (!usuario) return res.render('session/login', {
      info
    });
    req.logIn(usuario, function (err) {
      if (err) return next(err);
      const token = jwt.sign({id: usuario._id}, req.app.get('secretKey'), { expiresIn:'7d' });     
      console.log('Token generado es ',token)    
     return res.redirect('/any/'+token);
    });
  })(req, res, next);
});

app.get('/logout', function (req, res, next) {
  req.logOut(); //el framework lo hace solo
  res.redirect('/out');
});

app.get('/forgotPassword',function(req, res){
  res.render('session/forgotPassword');
})

app.post('/forgotPassword', function (req, res, next) { 
  Usuarios.findOne({ mail: req.body.email }, function (err, usuario) {    
    if (err) return next(err);
    if (usuario===null) return res.render('session/forgotPassword', {
      info: {
        message: 'No existe el email para el usuario ingresado'
      }});
   
    usuario.resetPassword(function(err){
        if(err)return next(err);
        console.log('Se envio a ','/session/forgotPasswordMessage');
    })
    res.render('session/forgotPasswordMessage');

  });
});

//reiniciar contraseña
app.get('/resetPassword/:token', function (req, res, next) {
  console.log('el token es '+req.params.token)
  Token.findOne({token:req.params.token},(err,token)=>{
      if(err) res.status(400).send({type:'not-verified',message:'No existe usuario asociado al token.Verifique que su token no haya expirado'});

      Usuarios.findById(token.usuario_Id,function(err,usuario){
        if(usuario===null){
          res.status(400).send({message:'No existe un usuario asociado al token'});          
        }
        res.render('session/resetPassword',{errors:{},usuario : usuario});
      });
  });   
});

//reiniciar contraseña
app.post('/resetPassword', function (req, res, next) {
  console.log('el body es',req.body);
  if(req.body.pwd!==req.body.confirm_pwd){
    res.render('session/resetPassword',
    {
      errors  : { confirm_pwd: { message:'No coinciden con el password ingresado' } },
      usuario : new Usuarios({mail:req.body.mail})
    });
  return;
  }
  Usuarios.findOne({ mail: req.body.email }, function (err, usuario) {  
    console.log('usuario encontrado',usuario);
    if(err){
      console.log('error en la consulta del cliente');
    }else{
      if(usuario.password!==null){
        usuario.password=req.body.pwd
        usuario.save(function(err){
          if(err){
            res.render('session/resetPassword',
            {
              info    : { message:err.errors },
              usuario : new Usuarios({mail:req.body.mail})
            });
          }else{
            res.redirect('/login')
          }
        });
      }
    }
  })
});
//############FIN################

//############Validacion de logeo pagina web################
function loggedIn(req,res,next){
  if(req.user){
    console.log('usuaria logeado');
    next(); //hace el sigueinte paso
  }
  else{
    console.log('usuaria no logeado');
    res.redirect('login')
  }
};
//############Fin de la validacion pagina web# ###############

//############Validacion de logeo API################
function validationUsuario(req,res,next){
  //verificamos el headers
  //toma la variable secretkey que es la variable que contiene el token
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'),function(err,decoded){
    if(err){
      res.json({ status:"error",message:err.message, data:null });
    }else{
      //va a viajar como payload para la comunicacion
      req.body.userIdToken=decoded.id;
      console.log('Usuario logeado en api ',decoded);
      next();
    }
  });

}
//############Fin de la Validacion  #################

//############logeo por google#################
app.get('/auth/google',
  passport.authenticate('google', { scope: 
      [ 'https://www.googleapis.com/auth/plus.login',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email' ] } //'https://www.googleapis.com/auth/plus.profile.emails.read'
));

app.get('/auth/google/callback', passport.authenticate( 'google', { failureRedirect: '/error'}),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
});
//############logeo por Facebook################
app.get('/auth/facebook', passport.authenticate('facebook',{
  authType: 'rerequest',
  scope: ['user_friends', 'email', 'public_profile'],
}));

app.get('/auth/facebook/callback',passport.authenticate('facebook',
  { successRedirect: '/',failureRedirect: '/login' })
);


app.use('/', indexRouter);
app.use('/bicicletas',loggedIn, bicicletasRouter); //primero valida si esta logeado y continua 
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter)
app.use('/login', loginRouter)
app.use('/register', registerRouter);

app.use('/privacy_policy',function(req,res){
  res.sendFile('public/privacy_policy.html')
});
//validar dominio
app.use('/google29f52070ac14cb1c',function(req,res){
  res.sendFile('public/google29f52070ac14cb1c.html')
})

//#######API#######
app.use('/api/auth',authApiRouter);
app.use('/api/bicicletas',validationUsuario, bicicletasRouterAPI);
app.use('/api/usuarios', usuarioRouterAPI);
//#######END#######



// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;