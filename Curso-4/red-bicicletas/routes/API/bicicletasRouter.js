var express = require('express');
var router = express.Router();
var bicicletasController = require('../../controllers/API/bicicletaControllerAPI'); //obtener controller


router.get('/', bicicletasController.bicicleta_list);//get
router.post('/create', bicicletasController.biclicte_create);//create
router.delete('/delete', bicicletasController.biclicte_delete);//create
router.put('/update', bicicletasController.biclicte_update);//update

 
module.exports = router;