var mymap = L.map('main_map').setView([4.6336951, -74.1584591, 11], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
    /* maxZoom: 18,
     id: 'mapbox/streets-v11',
     tileSize: 512,
     zoomOffset: -1,
     accessToken: 'your.mapbox.access.token'*/
}).addTo(mymap);


/*L.marker([4.6336951, -74.1584591, 11]).bindPopup("<b>Ciclista!</b><br>Estoy Bien!.").openPopup().addTo(mymap);
L.marker([4.6416355, -74.1486881, 16.88]).bindPopup("<b>Ciclista!</b><br>Estoy Bien!.").openPopup().addTo(mymap);
L.marker([4.6465931, -74.1563613, 18.29]).bindPopup("<b>Ciclista!</b><br>Estoy Bien!.").openPopup().addTo(mymap);
*/
var hidden = false;
function action(hidden) {   
    if (!hidden) {
        document.getElementById('salir').style.visibility = 'hidden';
    } else {
        document.getElementById('salir').style.visibility = 'visible';
    }
}





//solicitud a api
if (window.location.pathname.includes('/any/')) {
    var any = window.location.pathname.replace('/any/', '')
    if (any.length >= 30) {
        action(true);
        $.ajax({
            dataType: 'json',
            tupe: 'GET',
            headers: {
                "x-access-token": any
            },
            url: '/api/bicicletas',

            success: function (result) {
                console.log(result);
                if (result != undefined) {
                    result.bicicletas.forEach(function (bici) {
                        console.log(bici);
                        L.marker(bici.ubicacion, {
                            title: bici.id
                        }).bindPopup(`<b>Ciclista N°: ${bici.id}</b><br>Estoy Bien!.`).openPopup().addTo(mymap);
                    });
                }
            }
        })
    } else {
        action(false);
    }
}else{
    action(false);
}