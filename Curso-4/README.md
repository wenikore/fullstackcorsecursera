# Semana 1
## clases de node
 npm install instalar dependencias 
 
 instalar express para generar archivo
 sudo  npm install -g express-generator

 crear un proyecto
 express --view=pug myapp

para ejecutar 
npm start
 
recargar el servidor o sincronizacion de archivos
    npm install nodemon --save-dev

correr aplicacion modo debugger  colocar en el .json
    DEBUG=red-bicicletas:* npm run devstart

    https://leafletjs.com/examples/quick-start/  manejo de mapas
    https://html-to-pug.com/                     pasar de html a pug

crear pagina
    Visita el sitio Boostrap Themes: https://startbootstrap.com/themes/
    Descarga grayscale ( https://startbootstrap.com/themes/grayscale/ ) u otro template que te guste. 
    Configura en tu proyecto creado en la práctica anterior. 
    Utiliza el sitio https://html-to-pug.com/ para pasar del html al pug. Puedes usar otra herramienta similar, si te resulta más cómodo. 
    Levanta el servidor y revisa que se vea correctamente, comparándolo con la versión original. 
    Realiza un commit con los cambios realizados.

## Semana-2 
Durante este módulo trabajaremos fuertemente con temas de persistencia, utilizando Mongo DB, una base de datos orientada a documentos. Para facilitar el uso de las operaciones típicas de consulta, escritura, actualización y eliminación, utilizaremos un ODM (object document mapper), llamado Mongoose. Más allá de la persistencia y las herramientas que utilicemos, empezaremos a desarrollar con mayor profundidad nuestro modelo.

Para que este avance sea sólido y de calidad profesional, introduciremos conceptos básicos de testing y testing automático; solo será superficialmente y muy orientado al uso, pero pienso que es suficiente para que tomes conciencia de la importancia del testing y sus bondades. Más adelante podrás avanzar por tu cuenta si te interesa.

Nos apoyaremos en la herramienta Jasmine; si bien hay otras, la encontramos bastante simple y práctica para comenzar a escribir nuestros tests.

Al finalizar el módulo estarás en condiciones de persistir tu modelo en una base Mongo, tanto en desarrollo como en producción, y de armar tus propios tests automáticos.

## Jasmine testing
Primero, instalarlo globalmente para poder ejecutarlo directamente desde la terminal.
1) npm install –g jasmine
Luego, lo agregamos como dependencia desarrollo.
2) npm install --save-dev jasmine
Luego, debemos inicializar el módulo haciendo:
3) node node_modules/Jasmine/bin/Jasmine init
Y finalmente, agregamos una tarea en el package.json, en la sección de scripts:
4) “scripts”: {“test”: jasmine}
De esta forma, si ejecutamos npm test, se corren todos los tests que tengamos en
nuestro proyecto

ejecutar pruebas 
npm test
jasmine spec/API/bicicletas_api_test.spec.js 


para hacer test a las apis se instala

npm install request --save

## leccion 3 
npm install mongoose
npm install moment --save

Ejecutr test 
 jasmine spec/models/bicicletas_test.spec.js 
 jasmine spec/models/usuarios_test.spec.js 

 jasmine spec/API/bicicletas_api_test.spec.js 


## Semana 3
instalar encriptador de contraseñas 
npm install bcrypt --save 

--ayuda a gestionar datos unicos 
      npm i mongoose-unique-validator --save

Guía práctica de Autenticación 2: Mailing
Imitando lo que viste en el reciente tutorial, realiza los pasos a continuación.  

Recuerda que, para comprobar cuánto has aprendido, al final del curso otros participantes medirán tu avance en estas prácticas. 

Crea una cuenta de prueba en ethereal: https://ethereal.email  
Instala el paquete nodemailer.  
Configura el uso de mails, siguiendo los pasos enumerados en el tutorial, dentro de una carpeta de nombre “mailer”.  
Prueba enviar un email escribiendo algún método que quieras y, para asegurarte que esté funcionando correctamente, ejecútalo en algún paso del proyecto. 
Crea un nuevo commit con los avances realizados. 

npm install nodemailer --save

# leccion 2 Usando Passport para login local
 npm install passport  --save
 npm install passport-local  --save estrategia de logeo
 npm install express-session --save

 # leccion 3 jwt 
  npm install jsonwebtoken --save

## semana 4 
git init
heroku git:remote -a red-bicicletas-coursera2
git add *
git commit -m "deploy heroku"
git push heroku master
git pull heroku master
heroku open abre el browser

admin
rdYXd01T6gab558R

manejo de variables de entorno
Utilizando npm, instala la  librería “dotenv”. 
2. Agrega al archivo .gitignore la siguiente linea: “.env” para evitar que el repositorio git haga seguimiento del archivo generado. 
3. En el archivo .env agrega las siguientes variables de ambiente: 
NODE_ENV = “development” 
MONGO_URI=”[tu dirección de mongodb local]” 
4. En Heroku, dirígite a la vista de la aplicación y ubícate en la pestaña de Settings, luego ve a Config Vars y agrega las siguientes variabl
NODE_ENV=”production” 
MONGO_URI=”[dirección de conexión con mongo atlas]” 
5. Agrega un require(‘dotenv’) al comienzo del archivo app.js. 
6. Reemplaza el valor de la conexión a mongo con la variable MONGO_URI configurada en .env. 
7. Prueba la aplicación para asegurarte de que se utiliza correctamente el mongo local. 
8. Crea un nuevo commit y pushea a Bitbucket.
9. Haz un deploy a Heroku y prueba la aplicación productiva para asegurarte de que utilice el mongo atlas. 
npm install dotenv  --save proporcion aun archivo de configuracion


enviar correo por
Sendgrid

npm i  nodemailer-sendgrid-transport --save
Utilizando npm, instala la librería “nodemailer-sendgrid-transport”. 
Entra al sitio https://sendgrid.com y  créate una cuenta gratuita. 
Crea una API Key y guarda el secret que se genere en un lugar seguro. 
Agrega, en las variables de ambiente de Heroku, una entrada llamada SENDGRID_API_SECRET con el secret generado en sendgrid. 
 En el proyecto, en el archivo mailer.js , agrega la referencia instalada en el punto 1 y programa el envío de mails por sendgrid solo cuando estás ejecutando en el ambiente productivo. Revisa el video tutorial de sendgrid si tienes dudas. 
Prueba que el ambiente local sigue enviando los mails por ethereal.
Publica la aplicación a Heroku y prueba que los emails se envían por Sendgrid. Puedes crear un usuario con un email tuyo ya que esa acción envía un email de verificación.

#consola google para auth api
--Enlace de politicas 
--->condiciones de uso y emas con abogado.

reiniciar servicios heroku heroku restart

npm install connect-mongodb-session --save  --guardar session en mongo
npm install passport-google-oauth20

1. Ingresa al sitio de desarrolladores de Google y crea un proyecto.

2. Sobre el panel izquierdo, toca la sección de “Credenciales” y luego crea una credencial de “ID de cliente Oauth” 

3. Agrega la política de privacidad a tu proyecto y hazla accesible desde la web.  

4. Publica en Heroku.

5. Completa los datos tal como lo hemos hecho en el video.

Elige una imagen de la aplicación con las condiciones que pide Google y súbela al formulario.
Los dominios solicitados son los de Heroku de tu cuenta.
Agrega la ruta a la política de privacidad antes creada.
6. Crea una credencial de ID de cliente OAuth 2.0 de tipo “OTRO” y guarda en tu .env y en Heroku las claves secret y de cliente.  

7. Luego, en la consola de Google, dirígete a la sección de verificación de dominio y añade el dominio de tu APP en Heroku. 

8. Sigue los pasos que solicita Google para verificar el dominio utilizando los métodos de archivo, tal como lo hicimos en el video. 

Esto implica descargar el archivo de Google.
Agregarlo a tu proyecto y crear las rutas correspondientes para que lo accedan.
Publicar en Heroku.
9. Si haz configurada el dominio correctamente, deberás ver el dominio en el listado de dominios verificados. 

Utilizando npm instala “connect-mongodb-session” y “passport-google-oauth20” 
Agrega el require correspondientes en app.js para la librería connect-mongodb-session 
Programa la persistencia de sesiones para que en el ambiente local utilice MemoryStore y en producción MongoDBStore 
En app.js, agrega las rutas correspondientes para gestión el OAuth con Google. Sugiero que revises el video tutorial y la documentación de Passport para OAuth 2.0 con Google 
En el archivo Passport.js agrega la referencia a la librería de Passport para OAuth de Google, y programa la estrategia correspondiente. 
En el modelo de usuario crea el método “findOneOrCreateByGoogle” para utilizarlo en la estrategia de OAuth con Google definida en Passport.js. El objetivo del método es buscar o crear el usuario que solicitamos a Google. Vas a necesitar agregar algunos atributos al usuario para gestionar el id de Google. Sugiero fuertemente que revises el video tutorial en este punto. 
Dirígete a la página de desarrolladores de Google y crea una credencial de tipo Web. Asegúrate es escribir correctamente los callbacks.  
Actualiza en Heroku y tu ambiente local, el ID de cliente y el secret que se acaban de generar. 
Crea una API sin restricciones  
En el panel de control, habilita las APIs de Google+ y People 
En el proyecto Nodejs, agrega un link al login con Google en la pantalla de Login.  
Publica a Heroku y prueba la solución 


# Autenticacion con facebook
npm i passport-facebook-token
Entra al sitio https://developers.facebook.com y crea una aplicación para tu proyecto.
Luego, en la vista de la aplicación creada, clickea en “Integrar el inicio de sesión con Facebook” y dale al botón de “Confirmar” .
Guarda el identificador de la App y la clave secreta para agregarla en las variables de ambiente de Heroku y en tu archivo .env 
Completa los campos solicitados de la aplicación de Facebook. Puedes usar el mismo icono de aplicación que utilizaste para Google.
Utilizando npm, agrega la librería “Passport-facebook-token”.
Configura Passport.js para que maneje la estrategia de validación por token de Facebook. Esto implica que agregues el atributo de facebookID en el modelo de usuario y el método correspondiente de buscar o crear un usuario en base a los datos obtenidos de Facebook. 
En la api “authController” agrega los métodos de verificación de token de Facebook.
En la ruta de autorización de API, utiliza Passport para validar el token de Facebook.
Publica a Heroku.
En la vista de tu aplicación de Facebook, dirígete al panel izquierdo y dentro de la sección de Roles, ve a “Usuarios de prueba”.  
Agrega un usuario de prueba con permisos sólo de email. 
Toca en el usuario creado el botón editar y elige la opción “Obtener token de acceso para este usuario de prueba”.
Guarda el token generado para la siguiente prueba.
11. Utilizando Postman, verifica la validación del token de Facebook creado anteriormente. 

Revisa el video tutorial donde mostramos cómo armar el body con el token.
Prueba tanto contra el ambiente local como contra Heroku.
