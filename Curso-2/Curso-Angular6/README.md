# los paquetes a instalar son

    instalar  nvm que es un gestor de version de node
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.30.2/install.sh | bash\n
    instalar node
    nvm install node --reinstall-packages-from=node
    instalar typescript
    npm install -g typescript
    instalar node
    npm install -g @angular/cli
    ng --version
    instalar git

# crear un proyecto angular
    
    Crea un proyecto angular 
    ng new  ##nombreproyecto

    levantar servidor angular 
    ng serve
    ng serve --port 9090

    crear componentes     
    ng generate component #nombrecomponente

    instalar boostrap localmente en nuestro proyecto     
       npm install popper.js@^1.16.0 --save
       npm install jquery@3.5.0 --save
       npm install bootstrap@4.4.1 --save        
    ~  //buscar en el directorio
    

# Crear archivos de produccion
https://angular.io/guide/deployment
Este comando le dice a la herramienta ng que construya nuestra aplicación para un entorno de
producción.
ng build --prod --output-path docs --base-href /<project_name>/ funciona esta por la version de angular
ng build --target = production --base-href /

También establecemos la --base-href a una sola barra /.
La base-href describe cuál será la URL "raíz" de nuestra aplicación.
Así, por ejemplo, si quisieras para implementar su aplicación en una subcarpeta en su servidor en
/demo/, puede basar --base-href /demo/

creara una carpeta dist con la distribuccion

para deplegar en web crear cuenta en 
https://vercel.com
instalar
npm install -g now
y desplegar con 
now 
https://vercel.com/wenikore


	npm	install	-g	tsun

Code scaffolding
Run ng generate component component-name to generate a new component. You can also use ng generate directive|pipe|service|class|guard|interface|enum|module.



npm i @ngrx/effects@6.1.2 --save
npm i @ngrx/store@6.1.2 --save
ng add @angular/material


# Semana 3
objetos que permite indicar si la condicion de la app activa o no activa el guardian

    ng g guard  guard/usuario-logueado/usuario-logueado -->guard
    ng g s service/auth     ->crear servicio
    ng g c components/login/login --module app
    ng g c components/protected/protected  --module app  ---> proteger contenido sencble 

    rutas avanzadas
    ng g c components/vuelos/vuelos-component   --module app
    ng g c components/vuelos/vuelos-main-component  --module app
    ng g c components/vuelos/vuelos-mas-info-component  --module app
    ng g c components/vuelos/vuelos-detalle-component  --module app

    crear modulos
    ng g m reservas/reservas  --module app --flat --routing ( integra al codigo app modulo y routing para utilizar un ruteo especifio  --flat crea vinculacion en el modulo con  ReservasRoutingModule)
    ng g c reservas/components/reservas-listado
    ng g c reservas/components/reservas-detalle

## leccion 2 
    en el constructor se recibe la dependencia de un objeto creado y angular lo inyecta  y lo cra por los injectos  y provedores

    ng g s reservas/service/reservas-api-client

    para ver las tres formas de inyeccion  ver archivo destino-detalle.component.ts

## leccion 3
    API rest con node.js

    mkdir express-server
    npm init             -->inicializar node --nombre paquete y seguir pasos
    npm install express  --save  installar express
    npm install cors     -- installar cors para conexion de rest api por medio de puertos y aceptar peticciones
    crear y programar app.js
    node app.js --correr servidor

    instalar indexdb
    npm install dexie --save
    https://dexie.org/docs/Typescript
    https://github.com/dfahlander/Dexie.js
    https://w3c.github.io/IndexedDB/#key-path-construct


    integrador -->internacionalizacion de lenguaje  
    npm install @ngx-translate/core --save
    https://github.com/ngx-translate/core

# leccion 4 mapas

    https://github.com/Wykks/ngx-mapbox-gl
    https://www.mapbox.com/
    npm install ngx-mapbox-gl mapbox-gl@0.54.0 --save
    npm install @types/mapbox-gl@0.51.10 --save-dev  para typescript utilizar este tambien 
 
    improtan en los css globales

    colocar el imor en el modulo
    1) Instala los paquetes de npm para mapas de mapbox

1. npm install ngx-mapbox-gl mapbox-gl@0.49.0 --save
2. npm install @types/mapbox-gl@0.49.0 --save-dev
2) Importa los css globales
1. @import "~mapbox-gl/dist/mapbox-gl.css";
2. @import "~@mapbox/mapbox-gl-geocoder/lib/mapbox-gl-geocoder.css";

# leccion 2 
        directivas 
        ng g d  /directivas/espiame  --module app
        ng g d /directivas/tranckear-click  --module app

        5) Opcional: Usando un servicio que se instancie en app.module.ts, registra un observable sobre el store de redux que, ante un cambio en la cuenta de tags, loguee por consola o notifique a un servidor.

## Leccion 3
 testing con jasmine  
    ver destino-viaje.state.model.spec.ts

    para testear se ejecuta
    ng test --> corre los test y lavanta un chrome
    y usa la libreria karma  donde abre un chrome y corre el test


## Leccion 4
 cypress js
 realizar pruebas funcionales de incio a fin (end to end)
    instalar
    npm install cypress --save-dev
    cambiar configuracion en package.json
    cambiar e2e por cypress open

    y validar que funciones 
    ng serve
    npm run e2e --levanta un navegador
    1) Instala paquete de cypress con npm: npm install cypress --save-dev

2) Configura en package.json de modo que, al ejecutar el subcomando e2e, se ejecute “cypress open”.

3) Ejecuta la aplicación con ng serve para poner en marcha la aplicación angular, y también poner en marcha el api en express, ya que cypress debe acceder a un servidor con la aplicación funcionando.

4) Ejecuta npm run e2e para que se abra la interfaz de cypress.

5) Primero navega los ejemplos de cypress para probar diferentes pruebas que realiza la herramienta.

6) Luego, escribe al menos 3 tests de cypress propios, que prueben nuestra aplicación angular.

## automatizacion con cirlce ci para testing
 es una herramienta web  que es version gratis
 https://circleci.com/

 1)  En un navegador web:

Ingresa a circleci.com, registrarse.
Vincula cuenta de circleci a bitbucket o github según corresponda.
Crea job en circle ci para nuestro repositorio, creado en el módulo 1.
2) En nuestro proyecto, crea el archivo .circleci/configu.yml, tomando como referencia el anexo 1 al final de este archivo.

3) Modifica el archivo karma.conf.js según el anexo 1 al final de este archivo.

4) Commitea todos los cambios de nuestro repositorio, y haz “git push” a nuestro servidor de git.

5) Ve a circle ci y observa si el job se ejecuta correctamente.

ANEXO 1
    -- se debe crear archivo config.yml

Agregar .circleci/config.yml en la raíz del repositorio
	version: 2
	jobs:
	  build:
	    working_directory: ~/myapp
	    docker:
	      - image: circleci/node:8-browsers
	    steps:
	      - checkout:
				path: ~/myapp
	      - restore_cache:
	          key: angular-whishlist-{{ .Branch }}-{{ checksum "package-lock.json" }}
	      - run: npm install
	      - save_cache:
	          key: angular-whishlist-{{ .Branch }}-{{ checksum "package-lock.json" }}
	          paths:
	            - "node_modules"
	      - run: npm run test -- --progress=false --watch=false --browsers=ChromeHeadlessCI
          
Modificar en karma.conf.js

autoWatch: false,
	browsers: ['Chrome', 'ChromeHeadless', 'ChromeHeadlessCI'],
	customLaunchers: {
	  ChromeHeadlessCI: {
	    base: 'ChromeHeadless',
        flags: ['--no-sandbox', '--disable-gpu', '--disable-translate', '--disable-extensions', '--remote-debugging-port=9223']
	  }
	}