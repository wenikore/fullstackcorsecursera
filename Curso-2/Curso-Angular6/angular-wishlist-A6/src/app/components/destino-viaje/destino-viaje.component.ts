import { Component, OnInit,  Input,  HostBinding, EventEmitter, Output } from '@angular/core'; //recibir parametros externos input
import {  DestinoViaje } from './../../models/destino-viaje.models';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteDownAction , VoteUpAction, ResetVoteAction} from './../../models/destinos-viajes-state.model';
import { trigger, state, style, transition, animate } from '@angular/animations'

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations:[
    trigger('esFavorito',[
      state('estadoFavorito',style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNofavorito',style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito'  ,[animate('3s')]),
      transition('estadoFavorito   => estadoNofavorito',[animate('1s')])
      ]
    )
  ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje; //es posible que pase de una plantilla a otra y se recibe por input
  @Input('posicion') posicion:number;
  @HostBinding("attr.class") cssClass = "col-md-4"; // es un directiva que tiene una vinculas de un atributo 
  //string de nuestro componente que se vincula a los atributos del tag que se est envolviendo el contenido
  @Output() clicked:EventEmitter<DestinoViaje>;

  constructor(private store:Store<AppState>) { 
    this.clicked=new EventEmitter();
  }

  ngOnInit(): void {}

  ir(){
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp():any{
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown():any{
    this.store.dispatch(new VoteDownAction(this.destino));    
    return false;
  }

  resetVote():any{
    this.store.dispatch(new ResetVoteAction(this.destino));    
    return false;
  }


}
