import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../../service/auth.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  mensajeError:string;

  constructor(public authServices:AuthService) { 
    this.mensajeError="";
  }

  ngOnInit() {
  }

  login(username:string,password:string):boolean {
    this.mensajeError="";
    if(!this.authServices.login(username,password)){
      this.mensajeError="Login incorrecto...";
      setTimeout(function(){
        this.mensajeError="";        
      }.bind(this),2500);
    }
    return false;
  };

  logout():boolean{
    this.authServices.logout();
    return false;
  }
}
