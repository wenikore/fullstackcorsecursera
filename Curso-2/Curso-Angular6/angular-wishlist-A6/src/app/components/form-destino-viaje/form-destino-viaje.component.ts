import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.models';
import { FormGroup, Form, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent, Observable } from 'rxjs';
import { map,filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongNombre = 3;
  searchResult: string[];
  filteroptions:Observable<string[]>;
  //forwardRef == estamos inyectando para que los modulos sepan que se necesitan  porque se tine front y hace una referencia circular 
  //y lo paramos con este metodo 
  constructor(public fb:FormBuilder , @Inject(forwardRef(()=>APP_CONFIG))private config:AppConfig ) { //permite definir y construir grupo
    this.onItemAdded = new EventEmitter();
    this.fg          = fb.group({ //validator
      nombre   :['',Validators.compose([
                    Validators.required,
                    this.nombreValidator,
                    this.nombreValidatorParametrizable(this.minLongNombre)
                    ])],
      urlImagen:['',Validators.required]
    });

   /* this.fg.valueChanges.subscribe((form : any)=>{
      console.log("cambio el formulario " ,form);
    });*/    
   }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre,'input')//esucha las teclas es un observable
    .pipe(
      map((e:KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(120),
      distinctUntilChanged(),
      switchMap((text:string) => ajax(this.config.apiEndpoint+'/ciudades?q='+text)) //llamar servicio express
    ).subscribe(ajaxResponse => {
        this.searchResult = ajaxResponse.response        
        //.filter(function(x){ return x.toLowerCase().includes(elemNombre.value.toLowerCase())});
    });
  }



  guardar(nombre:string,urlImagen:string):boolean{
    let d= new DestinoViaje(nombre,urlImagen);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control:FormControl):{ [ control : string]: boolean } {
    let l = control.value.toString().trim().length;
    if(l > 0 && l < 5){
      return { invalidNombre : true };
    }
    return null;
  }

  //validador paramtrizable
  nombreValidatorParametrizable(minLong:number):ValidatorFn{
    return ( control:FormControl):{[control:string]:boolean}| null =>{
      let l = control.value.toString().trim().length;
      if(l > 0 && l < minLong){
        return { minLongNombre : true };
      }
      return null;
    }
  }


}
