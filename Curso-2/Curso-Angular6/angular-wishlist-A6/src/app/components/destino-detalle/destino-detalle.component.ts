import { Component, OnInit, InjectionToken,Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinoApiclient } from './../../models/destino-api-client.model';
import { DestinoViaje } from 'src/app/models/destino-viaje.models';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [ DestinoApiclient ]  //inyeccion por provider ejemplo 1

})
export class DestinoDetalleComponent implements OnInit {
  destino:DestinoViaje;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(private route:ActivatedRoute, private destinosApiclient:DestinoApiclient) { }

  ngOnInit(): void {
    let id= this.route.snapshot.paramMap.get("id");
    this.destino =this.destinosApiclient.getById(id);
    console.log('los dato son',this.destino);
  }

}
