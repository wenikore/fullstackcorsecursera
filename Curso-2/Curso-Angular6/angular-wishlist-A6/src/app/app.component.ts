import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-wishlist';
  hola    :string;
  listado :string;

  time = new Observable(observer =>{ //observable que le importa o le notifiquen el next
    setInterval(()=>observer.next(new Date().toString()),1000);
  });
  //rendereo diferido

  //internacionalizacion
  constructor(private translate:TranslateService){
    console.log("*********************get translate");
  /*  this.translate.get(['Hola', 'Listado'])
    .subscribe(translations => {
      console.log(translations['Hola']);
      console.log(translations['Listado']);
      this.listado=translations['Listado']
      this.hola=translations['Hola'];
    });*/

    translate.getTranslation('en')//.subscribe(x => console.log('x'+JSON.stringify(x))); //pedir traduccion ingles
    translate.setDefaultLang('es'); // establecer default español
  }

}
