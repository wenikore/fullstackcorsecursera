import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservasRoutingModule } from './reservas-routing.module';
import { ReservasListadoComponent } from './components/reservas-listado/reservas-listado.component';
import { ReservasDetalleComponent } from './components/reservas-detalle/reservas-detalle.component';
import { ReservasApiClientService } from './service/reservas-api-client.service';

@NgModule({
  imports: [
    CommonModule,
    ReservasRoutingModule
  ],  
  declarations: [ReservasListadoComponent, ReservasDetalleComponent],
  providers:[
    ReservasApiClientService
  ]
})
export class ReservasModule { }
