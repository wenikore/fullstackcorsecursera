import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service'

@Injectable({
  providedIn: 'root'
})
//canactivate=implementa metodo que permite activar los estado de la ruta y 
export class UsuarioLogueadoGuard implements CanActivate {

  constructor(public authServices:AuthService){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const isLoggedIn =this.authServices.isLoggedIn();
    console.log('conActivate',isLoggedIn);
    return isLoggedIn;
  }
}
