import { Directive , OnInit, OnDestroy} from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})
//oninit=inicializar evento
//ondestroy =destruir evento
export class EspiameDirective implements OnInit,OnDestroy{
  static nextId = 0;
  //constructor() { }

  log = (msg: string) => console.log(`Evento #${++EspiameDirective.nextId} ${msg}`);
  
  //implementacion de oninit
  ngOnInit(): void {
    this.log('##########******onInit ');
  }
  //implementacion de onDestroy
  ngOnDestroy(): void {
    this.log('##########******onDestroy ');
    }

}
