import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule ,ActionReducerMap, Store} from '@ngrx/store'
import { EffectsModule } from'@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { TranslateService, TranslateLoader, TranslateModule } from '@ngx-translate/core'
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinoComponent } from './components/lista-destino/lista-destino.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { 
  InitDataAction,
  DestinosViajesState,
  reducerDestinosViajes, 
  intializeDestinosViajesState,
  DestinosViajesEffects } from './models/destinos-viajes-state.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component'
import { UsuarioLogueadoGuard } from './guard/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './service/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { HttpClientModule, HttpHeaders, HttpRequest, HttpClient } from '@angular/common/http'; 
import Dexie from 'dexie';
import { DestinoViaje } from './models/destino-viaje.models';
import { EspiameDirective } from './directivas/espiame.directive';
import { TranckearClickDirective } from './directivas/tranckear-click.directive';



//inyeccion para llamar servicios
export interface AppConfig{
  apiEndpoint:String
}

const APP_CONFIG_VALUE:AppConfig={
  apiEndpoint:"http://localhost:3000"
}
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
//fin de inyeccion

// inicio route
const childrenRoutesVuelos : Routes = [
  { path: ''          ,   redirectTo : "main", pathMatch:'full'      },
  { path: 'main'      ,   component: VuelosMainComponentComponent    },
  { path: 'mas-info'  ,   component: VuelosMasInfoComponentComponent },
  { path: ':id'       ,   component: VuelosDetalleComponentComponent }
];


// rutas raiz
const routes : Routes = [
  { path: ''    ,      redirectTo : "home", pathMatch:'full'},
  { path: 'home',        component : ListaDestinoComponent   },
  { path: 'destino/:id', component : DestinoDetalleComponent },
  { path: 'login'     ,  component : LoginComponent          },
  { path: 'protected' ,  component : ProtectedComponent, canActivate:[UsuarioLogueadoGuard]  },
  //cargar rutas hijas
  { path: 'vuelos'    , 
    component:VuelosComponentComponent,
    canActivate:[UsuarioLogueadoGuard], //si esta logeado puedo ver la ruta
    children:childrenRoutesVuelos       //ruta hija que sera visible si es logeado
  } 
];
//fin route

// redux init
//definir estados de la aplicacion
export interface AppState{
  destinos:DestinosViajesState;
};
//definir reducer globales
const reducers:ActionReducerMap<AppState> ={
  destinos:reducerDestinosViajes
};
//inicializar 
let reducersInitialState={
  destinos: intializeDestinosViajesState()
}
// redux fin init

//app init llamada servicio
export function init_app(appLoadservices: AppLoadServices):() => Promise<any>{
  return() => appLoadservices.intializeDestinosViajesState();
}

@Injectable()
class AppLoadServices {
  constructor(private store:Store<AppState>,private http:HttpClient){}

  async intializeDestinosViajesState():Promise<any>{
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
    const request       = new HttpRequest('GET',APP_CONFIG_VALUE.apiEndpoint+'/my',{headers:headers});
    const response: any = await this.http.request(request).toPromise();
    this.store.dispatch( new  InitDataAction (response.body));

  }

}
//app fin
//internacionalizacion
export class  TranslationModel{
  constructor(
  public id:number,
  public lang:String,
  public key:any,
  public value:String
  ){}

}


//crear DB dixie
@Injectable({
  providedIn:'root'
})
class MyAppDatabase extends Dexie {
  destinosTable: Dexie.Table<DestinoViaje, number>; //crear tabla en indexdb
  translateTable: Dexie.Table<TranslationModel, number>; //crear tabla en indexdb de internacionalizacion
  constructor () {
    super("MyAppDatabase");
    this.version(1).stores({
      destinosTable: '++id, nombre, urlImagen ,votes'       //atributos de  mi objeto  
    });
    this.version(2).stores({
      destinosTable: '++id, nombre, urlImagen ,votes',
      translateTable:'++id, lang  , key       ,value '
    });


    this.destinosTable  = this.table("destinosTable");
    this.translateTable = this.table("translateTable");// almacenar los lenguaje y asi evitar de ir al API
  }
}
export const db= new MyAppDatabase();
//fin dixie
//internacionalizacion
//I18n inicia
  //implementa un cargador TranslateLoader
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    const promise = db.translateTable
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                                        if (results.length === 0) {
                                          return this.http
                                            .get<TranslationModel[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.translateTable.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        return results;
                                      }).then((traducciones) => {
                                        console.log('traducciones cargadas:');
                                        console.log(traducciones);
                                        return traducciones;
                                      }).then((traducciones) => {
                                        return traducciones.map((t) => ({[t.key]: t.value}))
                                      });   
   return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}

//I18n  finaliza





@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinoComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TranckearClickDirective    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes), //carga rutas raiz
    NgRxStoreModule.forRoot(reducers, { initialState:  reducersInitialState}),
    EffectsModule.forRoot([DestinosViajesEffects]),
    BrowserAnimationsModule,
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    HttpClientModule,
    TranslateModule.forRoot({ //importanto todo las traducciones a la pagina
      loader:{      //  utilizar un cargador
        provide    : TranslateLoader, //inyectrar dependencia 
        useFactory : (HttpLoaderFactory),//se va usar este factory
        deps       : [HttpClient]       //dependecia de httpclient
      }
    }),
    NgxMapboxGLModule
  ],
  providers: [
    AuthService,
    UsuarioLogueadoGuard,
    {provide:APP_CONFIG,useValue:APP_CONFIG_VALUE},
    AppLoadServices, 
    { provide:APP_INITIALIZER, useFactory:init_app, deps:[AppLoadServices], multi:true},
    //inyector token APP_INITIALIZER ,use factory que retorna un objeto que hace una tarea al inicializar
    //dependencia de apploadservices, multi: ejeucta multiples inicializacion
    MyAppDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
