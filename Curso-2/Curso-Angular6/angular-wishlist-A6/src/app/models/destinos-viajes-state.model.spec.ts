import {
  reducerDestinosViajes,
  DestinosViajesState,
  intializeDestinosViajesState,
  InitDataAction,
  NuevoDestinoAction,
  ElegidoFavoritoAction,
  VoteUpAction,
  VoteDownAction,
  ResetVoteAction
} from './destinos-viajes-state.model';
import {
  DestinoViaje
} from './destino-viaje.models';

describe('reducerDestinosViajes', () => {
  //test 1
  it('should reduce init data', () => {
    //setup :armar objetos para testiar
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: InitDataAction = new InitDataAction([new DestinoViaje('destinoTest1', 'destinourl1', 0), new DestinoViaje('destinoTest2', 'destinourl2', 0)]);
    //actiones: las acciones sobre el modelo o codigo
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    //assertins: verificaciones de codigo sobre el codigo estimulado linea 23 --> 
    //son las salidas esperadas
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('destinoTest1');
    //tear Down : borrar  y ejeuctar el codigo para borrar codigo insertado cuando se testea la base de datos    
  });
  //test 2
  it('should reduce new item added', () => {
    // setup
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url', 0));
    //actiones
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    //assertins
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('barcelona');
  });

  //test 3 is favorite
  it('should reduce select favorite item', () => {
    // setup
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: ElegidoFavoritoAction = new ElegidoFavoritoAction(new DestinoViaje('barcelona', 'url', 0));
    //actiones
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    //assertins    
    expect(newState.favorito.isSelected()).toEqual(true);
  });

  //test 4 vote up
  it('should  vote up for action', () => {
    // setup
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const create: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url', 0));
    //console.log('create',create.destino);
    const action: VoteUpAction = new VoteUpAction(new DestinoViaje(create.destino.nombre, create.destino.urlImagen, create.destino.votes));
    //console.log('action',action);
    //actiones
    const newStatecreate: DestinosViajesState = reducerDestinosViajes(prevState, create);
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    //assertins    
    //console.log('vote up',action.destino.votes );
    expect(action.destino.votes).toEqual(1);
  });

  //test 5 vote up
  it('should vote down for action', () => {
    // setup
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const create: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url', 0));
    //console.log('create',create.destino);
    const action: VoteDownAction = new VoteDownAction(new DestinoViaje(create.destino.nombre, create.destino.urlImagen, create.destino.votes));
    //console.log('action',action);
    //actiones
    const newStatecreate: DestinosViajesState = reducerDestinosViajes(prevState, create);
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    //assertins    
    //console.log('vote down',action.destino.votes );
    expect(action.destino.votes).toEqual(-1);
  });

  //test 6 vote up
  it('should reset for action', () => {
    // setup
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const create: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url', 0));
    //console.log('create',create.destino);
    const actionvoteup: VoteDownAction = new VoteDownAction(new DestinoViaje(create.destino.nombre, create.destino.urlImagen, create.destino.votes));
    const actionresert: ResetVoteAction = new ResetVoteAction(new DestinoViaje(create.destino.nombre, create.destino.urlImagen, create.destino.votes));

    //console.log('action',action);
    //actiones
    const newStatecreate: DestinosViajesState = reducerDestinosViajes(prevState, create);
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, actionvoteup);
    const newStatereset: DestinosViajesState = reducerDestinosViajes(prevState, actionresert);
    //assertins    
    //console.log('reset',actionresert.destino.votes );
    expect(actionresert.destino.votes).toEqual(0);
  });



});