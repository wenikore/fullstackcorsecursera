import {  DestinoViaje } from './destino-viaje.models';
import {  AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import {  Store } from '@ngrx/store';
import {
  DestinosViajesState,
  ElegidoFavoritoAction,
  NuevoDestinoAction
} from './destinos-viajes-state.model';
import {Injectable, Inject, forwardRef} from '@angular/core';
import { HttpClientModule, HttpHeaders, HttpRequest, HttpClient, HttpResponse } from '@angular/common/http'; 

@Injectable()
export class DestinoApiclient {
  destinos: DestinoViaje[]= [];

  constructor(private store: Store < AppState > ,
  @Inject(forwardRef(()=>APP_CONFIG))private config:AppConfig,
  private http:HttpClient)
  {    
    this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log("destinos sub store");
        console.log(data);
        this.destinos = data.items;
      });

    this.store
      .subscribe((data) => {
        console.log("all store");
        console.log(data);
      });
  }

  add(destino: DestinoViaje) { 
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
    const request  = new HttpRequest('POST',this.config.apiEndpoint+'/my',{nuevo:destino},{headers:headers});
    this.http.request(request).subscribe((data:HttpResponse<{}>)=>{
      if(data.status === 200){
        this.store.dispatch(new NuevoDestinoAction(destino));    
        //almacenar datos en indexdb
        const mydb = db;
        mydb.destinosTable.add(destino);
        console.log("imprimir todos los destrinos");
        mydb.destinosTable.toArray().then(destino=> console.log(destino)); //consulta y regresa una promesa, then lo recorremos imprimir data de DB
      }
    });  
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

  getById(id: string): DestinoViaje {
    return this.destinos.filter(function (data) {
    //  if (data.id == id) {
        return data;
      //}
    })[0];
  }

  elegir(destino: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(destino));
  }

}