import {  DestinoViaje } from './destino-viaje.models';
import {  AppState } from '../app.module';
import {  Store } from '@ngrx/store';
import {
  DestinosViajesState,
  ElegidoFavoritoAction,
  NuevoDestinoAction
} from './destinos-viajes-state.model';
import {Injectable} from '@angular/core';


@Injectable()
export class DestinoApiclient {
  destinos: DestinoViaje[]= [];

  constructor(private store: Store < AppState > ) {    
    this.store
      .select(state => state.destinos)
      .subscribe((data) => {
        console.log("destinos sub store");
        console.log(data);
        this.destinos = data.items;
      });

    this.store
      .subscribe((data) => {
        console.log("all store");
        console.log(data);
      });
  }

  add(destino: DestinoViaje) { 
    this.store.subscribe((data) => {        
      data.destinos.items.forEach(element => {
          element.setSelected(false);
      });  
    });    
    this.store.dispatch(new NuevoDestinoAction(destino));
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

  getById(id: string): DestinoViaje {
    return this.destinos.filter(function (data) {
      if (data.id.toString() == id) {
        return data;
      }
    })[0];
  }

  elegir(destino: DestinoViaje) {
    this.store.subscribe((data) => {        
      data.destinos.items.forEach(element => {
          element.setSelected(true);
      });  
    });   
    this.store.dispatch(new ElegidoFavoritoAction(destino));
  }

}