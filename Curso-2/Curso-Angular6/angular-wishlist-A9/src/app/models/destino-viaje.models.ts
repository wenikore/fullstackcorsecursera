import {v4 as uuid} from 'uuid';

export class DestinoViaje {
  selected: boolean;
  servicios: String[];
  id = uuid();
  constructor(public nombre: String, public urlImagen: String) {
    this.servicios = ["pileta", "desayunos"];
    this.selected  = false;
  }
  setSelected(isSelect: boolean){
    this.selected = isSelect;
  }

  isSelected(): boolean {
    return this.selected;
  }
 
}