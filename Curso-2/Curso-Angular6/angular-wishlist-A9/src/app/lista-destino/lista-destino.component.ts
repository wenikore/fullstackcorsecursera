import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { DestinoViaje} from './../models/destino-viaje.models';
import { DestinoApiclient } from './../models/destino-api-client.model';
import { DestinosViajesState } from './../models/destinos-viajes-state.model';
import { Store } from '@ngrx/store';
import {AppState} from './../app.module'

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css']
})
//lista de destino
export class ListaDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  update:string[];

  constructor(public destinoApiclient:DestinoApiclient, private store: Store<AppState>) {    
    this.onItemAdded = new EventEmitter();    
    this.update = [];    
  }

  ngOnInit(): void {
    this.store.select(state => state.destinos)
    .subscribe(data => {
      let d = data.favorito;
      if (d != null) {
        this.update.push("Se eligió: " + d.nombre);
      }
    });
  }

  agregado(destinonuevo:DestinoViaje){
    this.destinoApiclient.add(destinonuevo);
    this.onItemAdded.emit(destinonuevo);
   
  }
  
  elegido(destinoSeleccionado:DestinoViaje):void{
    this.destinoApiclient.elegir(destinoSeleccionado);    
  }

}
