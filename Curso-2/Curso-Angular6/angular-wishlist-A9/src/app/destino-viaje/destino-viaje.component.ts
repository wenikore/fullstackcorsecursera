import { Component, OnInit,  Input,  HostBinding, EventEmitter, Output } from '@angular/core'; //recibir parametros externos input
import {  DestinoViaje } from './../models/destino-viaje.models';


@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje; //es posible que pase de una plantilla a otra y se recibe por input
  @Input('posicion') posicion:number;
  @HostBinding("attr.class") cssClass = "col-md-4"; // es un directiva que tiene una vinculas de un atributo 
  //string de nuestro componente que se vincula a los atributos del tag que se est envolviendo el contenido
  @Output() clicked:EventEmitter<DestinoViaje>;

  constructor() { 
    this.clicked=new EventEmitter();
  }

  ngOnInit(): void {}

  ir(){
    this.clicked.emit(this.destino);
    return false;
  }
}
