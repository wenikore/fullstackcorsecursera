//inicializar servidor express
var express = require("express"); //requerir modulo
var cors = require("cors");
var app = express(); //inicializar
var port = 3000;
app.use(express.json()); //utilizar json
app.use(cors());

var ciudades = [
    "Paris",
    "Barcelona",
    "Barranquilla",
    "Montevideo",
    "Santiago de Chile",
    "Mexico DF",
    "Nueva York"
];
var miDestinos = [];

app.listen(port, () => console.log("Server running on port " + port)); //utilizar puerto 300
//peticciones 
app.get("/ciudades", (req, res, next) =>
    res.json(
        ciudades.filter((c) =>
            c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1
        )
    )
);

app.get("/my", (req, res, next) => res.json(miDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    miDestinos.push(req.body.nuevo);
    res.json(miDestinos);
});

app.get("/api/translation", (req, res, next) => res.json([
    { lang: req.query.lang , key:'Hola', value :' HOLA ' +req.query.lang },
    { lang: req.query.lang , key:'Listado', value :' LISTADO ' +req.query.lang }
]));