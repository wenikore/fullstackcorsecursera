//archivo de configuracion grunt
module.exports = function(grunt){
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt,{
        useminPrepare:'grunt-usemin'
    });
    
    
    grunt.initConfig({        
    //busca en todos los archivos css y los envie a la carpeta css y pone extencion css
        sass:{
            dist:{
                files:[{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'

                }]
            }
        },
        // tarea de monitoro css
        watch:{
            files: ['css/*.scss'],
            tasks: ['css'] 

        },
        // tare de configuracion de browser sync
        browserSync:{ // browser files
            dev:{
                bsFiles:{
                    src:[
                        'css/*.css',
                           '*.html',
                         'js./*.js'
                    ]
                },
                options:{
                    watchTask:true,
                    server   :{
                        baseDir:'./' //directorio base para nuestro servidor
                    }
                }
            }
        },
        // tarea de comprencion de imagenes
        imagemin:{
            dynamic:{
                files:[{
                    expand:true,
                    cwd: 'images/',
                    src: ['**/*.{png,jpg,gif,jpeg}'],
                    dest  :'dist/images'
                }]
            }
        },
        //pasar los html  a la distribuccion
        copy:{
            html:{
                files:[{
                    expand:true,
                    dot   :true,
                    cwd   :'./',
                    src   :['*.html'],
                    dest  :'dist'
                }]
            },
            fonts:{
                files:[{
                    expand:true,
                    dot   :true,
                    cwd   :'node_modules/open-iconic/font',
                    src   :['fonts/*.*'],
                    dest  :'dist'
                }]
            }         
            
        },
        //limpiar archivos
        clean:{
            build:{
                src:['dist/']
            }
        },
        //minificar css
        cssmin:{
            dist:{}
        },
        //ofuscar codigo
        uglify:{
            dist:{}
        },
        // va a generar el codigo hash para que no sea cacheable para los browser
        filerev:{
            options:{
                encoding  : 'utf8',
                algorithm : 'md5',
                length    : 20
            },
            //filerev:release hashes(md5) all assert(js, images css) in dist directoty
            release:{
                files:[{
                    src:[
                        'dist/js/*.js',
                        'dist/css/*.css'
                    ]
                }]
            }
        },
        //concatenar archivos
        concat:{
            options:{
                separator:';'
            },
            dist:{

            }
        },
        //usemin para  unificar los archivos
        //hace el proceeso de cssmin y uglify y los guarda en dist
        useminPrepare:{
            foo:{
                dest:'dist',
                src :['index.html', 'about.html','Contact.html','Payments.html', 'TermsConditions.html'] 
            },
            options:{
                flow:{
                    steps:{
                        css: ['cssmin'],
                        js : ['uglify']
                    },
                    post:{
                        css:[{
                            name:'cssmin',
                            createConfig:function(context,block){
                            var generated=context.options.generated;
                            generated.options={
                                keepSpecialComments:0,
                                rebase:false
                            }       
                            }
                        }]
                    }
                }

            }
        },
        //generar archivos y toma los assert
        usemin:{
            html:['dist/index.html','dist/about.html', 'dist/Contact.html','dist/Payments.html','dist/TermsConditions.html'],
            options:{
                assertDir:['dist','dist/css','dist/js']
            }
        }
    });
    //cargar libreria y registarla
   /* grunt.loadNpmTasks('grunt-contrib-watch')//libreria monitorear
    grunt.loadNpmTasks('grunt-contrib-sass');//libreria para compilar scss    
    grunt.loadNpmTasks('grunt-browser-sync');//libreria para syncronizar cambios    
    grunt.loadNpmTasks('grunt-contrib-imagemin');//libreria para compresion de imagenes*/
    grunt.registerTask('css'         ,['sass']);    //tarea de compilar css
    grunt.registerTask('img:compress',['imagemin']);//tarea de comprimir imagenes
    grunt.registerTask('default',['browserSync','watch']);  //tareas por defecto a ejecutar donde el va a subir el servidor y a monitorear
    grunt.registerTask('build',[
       'clean', //limpiar proyectos
       'copy',  //copiar archivos
       'imagemin',//minificar imagens
       'useminPrepare',//tarea
       'concat', //concatenar archivos
       'cssmin', //minificar css
       'uglify',//ofuscar codigo
       'filerev',//hash
       'usemin', //hacer distribuccion
       'default' //desplegar 
    ]);
};