# Guia de hoteles estudio
Este proyecto aprenderemos Boostrap 4

# Ejuectar proyecto
solo 
  npm run dev 
por hilos y monitoreando cambios en sass 
  npm run start

# forma de ejeuctar
npm install plugin --save(guardar en archivo)- ambiente

  $ npm install --save-dev copyfiles

# instalar dependencias en el package.json
       https://stackoverflow.com/questions/47039812/how-to-install-popper-js-with-bootstrap-4
       npm install popper.js@^1.16.3 --save
       npm install jquery@~3.4.1 --save
       npm install bootstrap@4.4.1 --save             
       npm install open-iconic --save
# armar proyecto
  npm install


# correr proyecto
  npm run dev

# Sass
  npm install node-sass --save-dev  
  para compilar con sass se ejeucta la siguiente tarea 
  la tarea esta en el package.json llamada scss
  npm run scss
  variables se declaran en $
  metodos con @mixin nombremetodo(variables); y lo llamamos  @include nombremetodo(variables);

# Less
  sudo npm install -g less --save
  para compilar con sass se ejeucta la siguiente tarea
  lessc css/styleless.less  css/styleless.css
  variables se declaran en @
  metodos con .nombremetodo(variables); y lo llamamos  .nombremetodo(variables);

# Implementando herramientas con NPM Scripts para automatizacion
  todos los cambios se hacen en Package.json
 ---
  onchange = permite monitorear los que son cambios en archivos y indicarle tareas de rutas cambiando en el html
  
  rimraf= borrado profundo de la carpeta dist de las versiones distribuibles
  npm install --save-dev onchange rimraf

  monitoreo-- cuando cambie ejecuta la tarea    
    "watch":"onchange  'css/*.css' -- npm run scss"
 ---
  npm install --save concurrently
  nos permite ejecutar tareas al mismo tiempo de forma concurrente y permite  en desarollo monitorear archivos y que el servidor escuche los cambios 

  "start":"concurrently \"npm run watch:scss\" \"npm run dev\" ",
------------------------------
--- programas de Distribuccion 
------------------------------
  permite copiar archivos de un lado al otro
  npm install --save dev copyfiles
  
  permite  comprimir imagenes y facilita la descarga del sitio web
  para que cargue de manera rapida
  sudo npm install -g imagemin-cli --unsafe-perm=true --allow-root

  usemin   =configurar y minificar y ofuscar el codigo css,jss y htlm minificado  
  uglifyjs = ofuscar el codigo js ,css espacios etc..
  cssmin   = minimiza los css
  htmlmin  = minifica los htlm
  npm install --save-dev usemin-cli cssmin uglifyjs htmlmin
  Recordar el usemin necesita reglas en el html
 <!-- build:js dist/index.js -->
  <!-- endbuild -->
  <!-- build:css dist/index.css -->
  <!-- endbuild -->
# documentacion
    https://www.npmjs.com/package/copyfiles

# Ejecutar build
  npm run build

# 2 de los task runners 
# Automatizadores de tareas Grunt
npm install grunt --save-dev 
1) crear archivo grunt  permite 
   configurar, y tenemos la configuracion de tareas
   touch Gruntfile.js

   instalar herramientas
1)pasar archivos sass a css
sudo npm install -g sass
     npm install -g grunt-cli
     npm install --save node-sass grunt-sass
     npm install grunt-contrib-sass --save-dev
     npm install --save-dev grunt-contrib-sass
documentacion guia para crear archivo
 https://github.com/gruntjs/grunt-contrib-sass
 https://sass-lang.com/install
 https://gruntjs.com/

 correr tarea el busca el archivo y la tare aregistrada
 grunt css
  -libreria para compilar  css npm install --save-dev grunt-contrib-sass
  -libreria para monitorear npm install --save-dev  grunt-contrib-watch
  -libreria que sincroniza los cambios en el browser
   npm install grunt-browser-sync --save-dev
  -libreria de comprencion de imagenes
   npm install grunt-contrib-imagemin --save-dev
  -liberia de tomar tiempos de ejecucion de tareas y cuanto se demora en ejecucion de tareas
   npm install time-grunt --save-dev
  -libreria que precargarla los modulos y permite ejeuctar las tareas mas rapidos
  npm install jit-grunt --save-dev
  -libreria   que copia archivos
  npm install grunt-contrib-copy --save-dev
  -libreria que limpia  carpetas
  npm install grunt-contrib-clean --save-dev
  -Libreria para concatenar archivos
  npm install grunt-contrib-concat --save-dev
  -libreria para minificar css
  npm install grunt-contrib-cssmin --save-dev
  -libreria para ofuscar el codigo
  npm install grunt-contrib-uglify --save-dev
  -Libreria para cargar y version  archivos y genera un codigo hash por archivos para que el  browser lo descargue nuevamente  para los servidores
  npm install grunt-filerev --save-dev
  -Libreria
  npm install grunt-usemin --save-dev
  -Libreria

para ejeuctar tareas con grunt ejeuctas grun Nombretarea
si tienes tareas default definidas solo grunt

# Automatizadores de tareas Gulp
ejeuctar tareas gulp nombretarea

instalar de forma global
sudo npm install -g gulp-cli
instalar gulp en nuestro proyecto
 npm install gulp --save-dev

--libreria ue compila scss a css
npm install gulp-sass --save-dev

 Crear archivo gulpfile.js configuracion de gulp

--libreria para sincronizar archivos al servidor
  npm install browser-sync --save-dev

 --libreria para  del  para eliminar carpertas y comprension de imagenes
 npm install del gulp-imagemin --save-dev
 --libreria  gulp-uglify ofuscar codigo
  --libreria gulp-usemin 
  --libreria gulp-rev ofuscar codigo
  --libreria gulp-clean-css
  --libreria gulp-flatmap
  --libreria gulp-htmlmin minifcacion

 npm install del gulp-uglify gulp-usemin gulp-rev gulp-clean-css gulp-flatmap gulp-htmlmin --save-dev